import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Song } from './Song';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, pluck, tap } from 'rxjs/operators';

import { EMPTY, of } from 'rxjs';
import { subscribeOn } from 'rxjs-compat/operator/subscribeOn';
import { reduce } from 'rxjs-compat/operator/reduce';
import { subscribeToArray } from 'rxjs/internal-compatibility';
import { switchMap } from 'rxjs-compat/operator/switchMap';
import { concatMap } from 'rxjs-compat/operator/concatMap';


@Injectable({
  providedIn: 'root'
})
export class LijstService {

  private lijst: Observable<Song[]>=EMPTY;
  private arrayLijst=[];
  private selectedSong: Song;
  private input: string = "";
  public filledInput:boolean=false;

  private headerDict = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Methods': 'GET',
    'Access-Control-Allow-Origin': '*'
  };
  private requestOptions = {
    headers: new HttpHeaders(this.headerDict)
  };
  constructor(private http: HttpClient) { }

  getLijst():Observable<Song[]> {
    return of(this.arrayLijst);
  }
  setLijst(lijst: Observable<Song[]>) {
    this.lijst = lijst;
  }
  setSelected(song: Song) {
    this.selectedSong = song;
  };
  getSelectedSong() {
    return this.selectedSong;
  };
  private mapResponseItem(item: any): Song {
    return (new Song(item.trackId, item.artistName, item.artworkUrl60, item.trackName, item.previewUrl));
  }

  searchSongs(inp: string): Observable<Song[]> {
    if(this.arrayLijst.length>0) this.arrayLijst=[];
    this.setInput(inp);
    if (inp != "") {
      return this.http.get<any>('/search?term='.concat(this.getInput()), this.requestOptions)
        .pipe(pluck('results'),
          map((data: any) => data.map((item: any) => {
                                                        const song=this.mapResponseItem(item);
                                                        this.arrayLijst.push(song);
                                                        this.filledInput=true;
                                                          return song;})));
    }
    else return of([]);
  }
  getInput() {
    return this.input;
  }

  setInput(inpu: string) {
    this.input = inpu;

  }

}
