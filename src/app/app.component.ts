import { Component, OnInit } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'iTunes music searcher!';
  constructor() { }

  ngOnInit(): void {

  }
}
