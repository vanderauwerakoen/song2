import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DetailsSongComponent } from './details-song/details-song.component';
import { LijstService } from 'src/lijst.service';
import { LijstComponent } from './lijst/lijst.component';
import {MatTableModule} from '@angular/material/table';
import { MatInputModule } from "@angular/material/input";

@NgModule({
  declarations: [
    AppComponent,
    DetailsSongComponent,
    LijstComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,HttpClientModule,MatTableModule,MatInputModule
  ],
  providers: [LijstService],
  bootstrap: [AppComponent]
})
export class AppModule { }
