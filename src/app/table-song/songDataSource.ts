import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { BehaviorSubject, concat, EMPTY, Observable, of, Subscription } from "rxjs";
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { LijstService } from "src/lijst.service";
import { Song } from "src/Song";
import { LijstComponent } from "../lijst/lijst.component";



export class SongDataSourcce implements DataSource<Song>{

  private songsSubject = new BehaviorSubject<Song[]>([]);

  arraySongs = [];
  subscription: Subscription;

  constructor(private lijstService:LijstService) {

    this.loadSongsFromService(lijstService.getLijst());
  }

  connect(collectionViewer: CollectionViewer): Observable<Song[]> {
    return this.songsSubject.asObservable();
  }
  disconnect(collectionViewer: CollectionViewer): void {

  }

  loadSongs(observ) {
    var obser: Observable<Song[]> = (observ);
    this.subscription= observ.subscribe(
      songs => {
        this.songsSubject.next((songs));
      });

  }

  loadSongsFromService(arg0: Observable<Song[]>) {
    this.subscription=arg0.subscribe(
      songs => {
        this.songsSubject.next((songs));
      });


  }



}
