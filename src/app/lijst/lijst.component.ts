
import { AfterViewInit, Component, ElementRef, Injectable, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { BehaviorSubject, fromEvent, from, Observable, Subject, concat } from 'rxjs';
import { Song } from 'src/Song';
import { debounceTime, distinctUntilChanged, switchMap, map, tap, pluck } from 'rxjs/operators';
import { LijstService } from 'src/lijst.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { of } from 'rxjs';

import { SongDataSourcce } from '../table-song/songDataSource';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-lijst',
  templateUrl: './lijst.component.html',
  styleUrls: ['./lijst.component.css']
})
@Injectable({ providedIn: 'root' })
export class LijstComponent implements OnInit, OnDestroy {

  lijst: Observable<Song[]> = null;

  obs;
  searching = "Please type in a filter ...";
  dataSource: SongDataSourcce;
  constructor(private lijstService: LijstService, private router: Router) { }

  ngOnDestroy(): void {
    if (!(this.dataSource.subscription == undefined))
      this.dataSource = null;
  }



  onKeyUp() {

    this.lijstService.filledInput = false;
    this.dataSource.loadSongs(this.lijst);
  }



  ngOnInit(): void {



    const inputlijst: HTMLInputElement = document.querySelector(".inputLijst");
    this.dataSource = new SongDataSourcce(this.lijstService);
    this.obs = fromEvent(inputlijst, 'keyup');
    this.lijst = this.obs.pipe(
      pluck('target', 'value'),
      //tap((val: string) => { console.log(val); return val }),
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((inp: string) => this.lijstService.searchSongs(inp)))

    if (this.lijstService.filledInput) {
      inputlijst.value = this.lijstService.getInput();
    }

    this.dataSource.loadSongs(this.lijst);

    inputlijst.focus();

  }




  goto(item: Song): void {
    this.lijstService.setSelected(item);
    this.router.navigate(['detail']);
  }

}

