import { Component, OnInit } from '@angular/core';
import { LijstService } from 'src/lijst.service';
import { Song } from 'src/Song';
import { Location } from '@angular/common';
import { Observable, fromEvent } from 'rxjs'

@Component({
  selector: 'app-details-song',
  templateUrl: './details-song.component.html',
  styleUrls: ['./details-song.component.css']
})
export class DetailsSongComponent implements OnInit {
  public selSong: Song;

  constructor(private servic: LijstService, private location: Location) {

  }

  ngOnInit(): void {
    this.selSong = this.servic.getSelectedSong();
    var goBack = document.querySelector('.back');
    var clickStream = fromEvent(goBack, 'click').subscribe((e)=>this.cancel());
  }
  cancel() {
    this.location.back(); // <-- go back to previous location on cancel
  }
}
