import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LijstService } from 'src/lijst.service';
import { AppComponent } from './app.component';
import { DetailsSongComponent } from './details-song/details-song.component';
import { LijstComponent } from './lijst/lijst.component';

const routes: Routes = [
  {path:'detail',component:DetailsSongComponent},
  {path:'lijst',component:LijstComponent},
  {path:'',redirectTo:'/lijst', pathMatch: 'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
