export class Song {
    id:number;
    name: string;
    foto: string;
    track: string;
    sound: string;

    public constructor(
      id:number,
        name: string,
        foto: string,
        track: string,
        sound: string) {
            this.id=id;
            this.name = name;
            this.foto = foto;
            this.track = track;
            this.sound = sound;
        }
}
